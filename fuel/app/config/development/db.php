<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=nettuts_fuel_blog',
			'username'   => 'root',
			'password'   => 'password',
		),
	),
);
